package ${packageName }


import java.util.Date;



public class ${tableMeta.modelName?cap_first} {

	<#list tableMeta.columnMetas as x>		
		${x.javaType} ${x.attrName};
	</#list>

	<#list tableMeta.columnMetas as x>		
		public ${x.javaType} get${x.attrName?cap_first}(){
			return this.${x.attrName};
		}		
		public void set${x.attrName?cap_first}(${x.javaType} ${x.attrName}){
			this.${x.attrName} = ${x.attrName};
		}
	</#list>
}
