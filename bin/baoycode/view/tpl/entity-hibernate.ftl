package ${packageName }


import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@Entity
@Table(name="${tableMeta.name}") 
public class ${tableMeta.modelName?cap_first} {

	<#list tableMeta.columnMetas as x>		
		${x.javaType} ${x.attrName};
	</#list>

	<#list tableMeta.columnMetas as x>		
		<#if x.isPrimaryKey??>
		@id
		@GeneratedValue(strategy=GenerationType,generator="UUID")
		</#if>
		public ${x.javaType} get${x.attrName?cap_first}(){
			return this.${x.attrName};
		}		
		public void set${x.attrName?cap_first}(${x.javaType} ${x.attrName}){
			this.${x.attrName} = ${x.attrName};
		}
	</#list>
}
