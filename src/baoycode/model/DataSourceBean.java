package baoycode.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataSourceBean implements Serializable {
	private static final long serialVersionUID = -3052251725300582257L;
	String conName;
	String dbName;
	String driver;
	String url;
	String user;
	String password;

	public String getDataBaseType() {
		if ("oracle.jdbc.driver.OracleDriver".equals(this.driver)) {
			return "ORACLE";
		} else if ("com.mysql.jdbc.Driver".equals(this.driver)) {
			return "MYSQL";
		} else {
			return "";
		}
	}

	public static DataSource toDataSource(DataSourceBean bean) {
		DruidDataSource ds = new DruidDataSource();
		ds.setUrl(bean.getUrl());
		ds.setUsername(bean.getUser());
		ds.setPassword(bean.getPassword());
		ds.setDriverClassName(bean.getDriver());
		return ds;
	}
	
	public static String[] getNames(Set<DataSourceBean> beans) {
		Set<String> names = new HashSet<String>();
		for (DataSourceBean bean : beans) {
			names.add(bean.getDbName());
		}
		return names.toArray(new String[names.size()]);
	}
}
