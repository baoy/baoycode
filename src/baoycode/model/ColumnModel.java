package baoycode.model;

import baoycode.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ColumnModel {
	String columnName;
	String javaName;
	String javaType;
	String dbType;// 数据库声明的类型
	int datasize;// 数据大小
	int digits;// 小数点位数
	String isNull;
	String isQuery;
	String isList;
	String isEdit;
	String comment;
	String isParamKey;
	String dictKey;// 数据字典ID

	/**
	 * 做部分字段初始化操作
	 */
	public void init() {
		// 设置JAVA属性名
		javaName = StringUtils.getJavaName(columnName);

		// 设置是否为主键
		if ("PRI".equalsIgnoreCase(isParamKey)) {
			this.isParamKey = "Y";
		}

		this.isQuery = "N";
		this.dictKey = "";

		this.isList = "Y";
		this.isEdit = "Y";

		String dbType = StringUtils.substringBefore(javaType, "(").toUpperCase();
		if ("VARCHAR".equals(dbType) || "CHAR".equals(dbType) || "VARCHAR2".equals(dbType)) {
			this.javaType = "String";
		} else if ("DATETIME".equals(dbType)) {
			this.javaType = "Date";
		} else if ("BIGINT".equals(dbType)) {
			this.javaType = "Long";
		} else if ("INT(11)".equals(dbType)) {
			this.javaType = "Integer";
		} else if ("DATE".equals(dbType)) {
			this.javaType = "java.util.Date";
		} else if ("TINYINT(4)".equals(dbType)) {
			this.javaType = "Integer";
		}
	}

	/**
	 * 做部分字段初始化操作,处理ORACLE数据库
	 */
	public void initOra() {
		// 设置JAVA属性名
		javaName = StringUtils.getJavaName(columnName);

		// 设置是否为主键
		if ("PRI".equalsIgnoreCase(isParamKey)) {
			this.isParamKey = "Y";
		}

		// 忽略掉部分字段DATA_STATUS,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE
		if ("DATA_STATUS".equalsIgnoreCase(columnName) || "CREATE_USER".equalsIgnoreCase(columnName)
				|| "CREATE_DATE".equalsIgnoreCase(columnName) || "UPDATE_USER".equalsIgnoreCase(columnName)
				|| "UPDATE_DATE".equalsIgnoreCase(columnName)) {
			this.isList = "N";
			this.isEdit = "N";
		} else {
			this.isList = "Y";
			this.isEdit = "Y";
		}

		this.isQuery = "N";
		this.dictKey = "";

		// 转换为大写比较
		this.dbType = this.dbType.toUpperCase();
		if ("VARCHAR".equals(dbType) || "CHAR".equals(dbType) || "VARCHAR2".equals(dbType)) {
			this.javaType = "String";
		} else if ("DATETIME".equals(dbType) || "DATE".equals(dbType)) {
			this.javaType = "Date";
		} else if ("BIGINT".equals(dbType)) {
			this.javaType = "Long";
		} else if ("NUMBER".equals(dbType) && digits == 0) {
			this.javaType = "Long";
		} else if ("NUMBER".equals(dbType) && digits > 0) {
			this.javaType = "Double";
		}

	}

	public boolean isKey() {
		return "Y".equals(this.isParamKey);
	}

	public boolean isNullAble() {
		return "Y".equals(this.isNull);
	}

}
