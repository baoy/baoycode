package baoycode;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import baoycode.utils.I18n;
import baoycode.utils.ViewCreator;

public class FillLayoutDemo2 {
	private Shell shell;
	Text driverClass;
	Text connectionName;
	Text url;
	Text username;
	Text password;
	private Button btnTest;
	private Button btnSave;

	public void open() {
		Display display = Display.getDefault();
		createContents();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void createContents() {
		shell = new Shell(SWT.SHELL_TRIM);
		shell.setText("数据库管理");
		shell.setSize(400, 240);
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		

		SashForm sashForm = new SashForm(shell, SWT.VERTICAL);
		Composite composite_1 = new Composite(sashForm, SWT.BORDER);
		GridLayout gl_composite_1 = new GridLayout(3, false);
		gl_composite_1.verticalSpacing = 10;
		composite_1.setLayout(gl_composite_1);

		connectionName = ViewCreator.createText(composite_1, I18n.databaseNameLabel);
		driverClass = ViewCreator.createText(composite_1, I18n.driverClassLabel);
		url = ViewCreator.createText(composite_1, I18n.jdbcUrlLabel);
		username = ViewCreator.createText(composite_1, I18n.usernameLabel);
		password = ViewCreator.createText(composite_1, I18n.passwordLabel);

		Composite composite_2 = new Composite(sashForm, SWT.BORDER);
		GridLayout layout = new GridLayout(3, false);
		composite_2.setLayout(layout);

		btnSave = ViewCreator.createButton(composite_2, "保存", 79);
		btnSave.setEnabled(false);

		btnTest = ViewCreator.createButton(composite_2, "测试连接", 79);
		btnTest.setEnabled(false);

		Button button = ViewCreator.createButton(composite_2, "关闭", 79);
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		sashForm.setWeights(new int[] { 180, 48 });
		shell.open();
	}

	public static void main(String[] args) {
		String userDir = System.getProperty("user.dir");
		System.out.println(userDir);
		FillLayoutDemo2 demo = new FillLayoutDemo2();
		demo.open();
	}
}