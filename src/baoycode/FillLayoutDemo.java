package baoycode;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class FillLayoutDemo {
	private Shell shell;
	private Table dataTable;
	private Text nameText;

	public void open() {
		Display display = Display.getDefault();
		createContents();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	
	protected void createContents() {
		shell = new Shell(SWT.MIN | SWT.CLOSE);
		shell.setText("简易信息管理");
		shell.setSize(300, 576);
		shell.setLayout(new FillLayout(SWT.VERTICAL));

		Composite compositeUp = new Composite(shell, SWT.NONE);
		compositeUp.setLayout(new FillLayout(SWT.HORIZONTAL));

		dataTable = new Table(compositeUp, SWT.BORDER | SWT.FULL_SELECTION);
		dataTable.setHeaderVisible(true);
		dataTable.setLinesVisible(true);

		TableColumn tblclmnId = new TableColumn(dataTable, SWT.NONE);
		tblclmnId.setWidth(39);
		tblclmnId.setText("ID");

		TableColumn tblclmnName = new TableColumn(dataTable, SWT.NONE);
		tblclmnName.setWidth(100);
		tblclmnName.setText("Name");

		TableColumn tblclmnSex = new TableColumn(dataTable, SWT.NONE);
		tblclmnSex.setWidth(100);
		tblclmnSex.setText("Sex");

		TableColumn tblclmnAge = new TableColumn(dataTable, SWT.NONE);
		tblclmnAge.setWidth(100);
		tblclmnAge.setText("Age");

		Composite compositeDoen = new Composite(shell, SWT.NONE);
		compositeDoen.setLayout(null);

		Group group = new Group(compositeDoen, SWT.NONE);
		group.setText("新增");
		group.setBounds(10, 10, 274, 254);
		group.setLayout(null);

		Button cancelBtn = new Button(group, SWT.NONE);
		cancelBtn.setBounds(166, 198, 61, 27);
		cancelBtn.setText("重置");

		Button addBtn = new Button(group, SWT.NONE);
		addBtn.setBounds(66, 198, 61, 27);
		addBtn.setText("新增");

		Label birthdayLabel = new Label(group, SWT.NONE);
		birthdayLabel.setBounds(10, 123, 53, 17);
		birthdayLabel.setText("Birthday");

		DateTime birthdayDate = new DateTime(group, SWT.BORDER);
		birthdayDate.setBounds(85, 123, 95, 24);

		Label sexLabel = new Label(group, SWT.NONE);
		sexLabel.setBounds(10, 77, 53, 17);
		sexLabel.setText("Sex");

		Button sexMRadio = new Button(group, SWT.RADIO);
		sexMRadio.setBounds(85, 77, 42, 17);
		sexMRadio.setText("男");

		Button sexFmRadio = new Button(group, SWT.RADIO);
		sexFmRadio.setBounds(138, 77, 42, 17);
		sexFmRadio.setText("女");

		nameText = new Text(group, SWT.BORDER);
		nameText.setBounds(85, 33, 179, 23);

		Label nameLabel = new Label(group, SWT.NONE);
		nameLabel.setBounds(10, 33, 53, 17);
		nameLabel.setText("Name");
		insertDates(dataTable);
		shell.open();
	}

	private void insertDates(Table dataTable2) {
		TableItem item = null;
		for (int i = 1; i < 100; i++) {
			item = new TableItem(dataTable, SWT.NONE);
			item.setText(new String[] { i + "", "学生" + i, (i % 2 == 0 ? "男" : "女"), i + 10 + "" });
		}

	}

	public static void main(String[] args) {
		FillLayoutDemo demo = new FillLayoutDemo();
		demo.open();
	}
}