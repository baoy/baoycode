package baoycode;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import baoycode.view.TemplateView;

public class FillLayoutDemo3 {
	private Shell shell;
	Text driverClass;
	Text connectionName;
	Text url;
	Text username;
	Text password;

	public void open() {
		Display display = Display.getDefault();
		createContents();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void createContents() {
		shell = new Shell(SWT.SHELL_TRIM);
		shell.setText("数据库管理");
		TemplateView dailog = new TemplateView(shell);
//		SQLView dailog = new SQLView(shell);
		dailog.open();
	}

	public static void main(String[] args) {
		FillLayoutDemo3 demo = new FillLayoutDemo3();
		demo.open();
	}
}