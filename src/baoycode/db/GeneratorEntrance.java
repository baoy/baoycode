package baoycode.db;

import java.util.List;

import javax.sql.DataSource;


public class GeneratorEntrance {

	MetaBuilder metaBuilder;
	JavaBeanGenerator javaBeanGenerator;

	public GeneratorEntrance(DataSource dataSource, String baseModelPackageName, String baseModelOutputDir) {
		this.metaBuilder = new MetaBuilder(dataSource);
		this.javaBeanGenerator = new JavaBeanGenerator(baseModelPackageName, baseModelOutputDir);
	}

	public void setDialect(Dialect dialect) {
		metaBuilder.setDialect(dialect);
	}

	public void setTypeMapping(TypeMapping typeMapping) {
		this.metaBuilder.setTypeMapping(typeMapping);
	}

	public void setRemovedTableNamePrefixes(String... removedTableNamePrefixes) {
		metaBuilder.setRemovedTableNamePrefixes(removedTableNamePrefixes);
	}

	public void addExcludedTable(String... excludedTables) {
		metaBuilder.addExcludedTable(excludedTables);
	}

	public void generate() {
		long start = System.currentTimeMillis();
		List<TableMeta> tableMetas = metaBuilder.build();
		if (tableMetas.size() == 0) {
			System.out.println("TableMeta 数量为 0，不生成任何文件");
			return;
		}
		javaBeanGenerator.generate(tableMetas);
		long usedTime = (System.currentTimeMillis() - start) / 1000;
		System.out.println("Generate complete in " + usedTime + " seconds.");
	}
}
