/**
 * Copyright (c) 2011-2017, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package baoycode.db;

import java.util.ArrayList;
import java.util.List;

import baoycode.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TableMeta {
	
	public String name;					// 表名
	public String remarks;				// 表备注
	public String primaryKey;			// 主键，复合主键以逗号分隔
	public List<ColumnMeta> columnMetas = new ArrayList<ColumnMeta>();	// 字段 meta
	
	// ---------
	
	public String baseModelName;		// 生成的 base model 名
	public String baseModelContent;		// 生成的 base model 内容
	
	public String modelName;			// 生成的 model 名
	public String modelContent;			// 生成的 model 内容
	
	// ---------
	
	public int colNameMaxLen = "Field".length();			// 字段名最大宽度，用于辅助生成字典文件样式
	public int colTypeMaxLen = "Type".length();				// 字段类型最大宽度，用于辅助生成字典文件样式
	public int colDefaultValueMaxLen = "Default".length();	// 字段默认值最大宽度，用于辅助生成字典文件样式
	@Override
	public String toString() {
		return "TableMeta [name=" + name + ", remarks=" + remarks + ", primaryKey=" + primaryKey + ", columnMetas="
				+ columnMetas + ", baseModelName=" + baseModelName + ", baseModelContent=" + baseModelContent
				+ ", modelName=" + modelName + ", modelContent=" + modelContent + ", colNameMaxLen=" + colNameMaxLen
				+ ", colTypeMaxLen=" + colTypeMaxLen + ", colDefaultValueMaxLen=" + colDefaultValueMaxLen + "]";
	}
	
	
	public static TableMeta getTableMetaByName(java.util.List<TableMeta> tableMetas, String tableName) {
		if (tableMetas != null && tableMetas.size() > 0) {
			for (TableMeta tm : tableMetas) {
				if (StringUtils.equals(tm.name, tableName)) {
					return tm;
				}
			}
		}
		return null;
	}
}




