
package baoycode.db;


public class OracleDialect extends Dialect {
	
	@Override
	public String forTableBuilderDoBuild(String tableName) {
		return "select * from " + tableName + " where rownum < 1";
	}
	
}
