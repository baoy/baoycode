/**
 * Copyright (c) 2011-2017, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package baoycode.db;

import java.util.HashMap;
import java.util.Map;

/**
 * TypeMapping 建立起 ResultSetMetaData.getColumnClassName(i)到 java类型的映射关系
 * 特别注意所有时间型类型全部映射为 java.util.Date，可通过继承扩展该类来调整映射满足特殊需求
 * 
 * 与 com.jfinal.plugin.activerecord.JavaType.java 类型映射不同之处在于将所有
 * 时间型类型全部对应到 java.util.Date
 */
public class TypeMapping {
	
	@SuppressWarnings("serial")
	protected Map<String, String> map = new HashMap<String, String>() {{
		// java.util.Data can not be returned
		// java.sql.Date, java.sql.Time, java.sql.Timestamp all extends java.util.Data so getDate can return the three types data
		// put("java.util.Date", "java.util.Date");
		
		put("oracle.sql.TIMESTAMP", "Date");
		
		// date, year
		put("java.sql.Date", "Date");
		
		// time
		put("java.sql.Time", "Date");
		
		// timestamp, datetime
		put("java.sql.Timestamp", "Date");
		
		// binary, varbinary, tinyblob, blob, mediumblob, longblob
		// qjd project: print_info.content varbinary(61800);
		put("[B", "byte[]");
		
		// ---------
		
		// varchar, char, enum, set, text, tinytext, mediumtext, longtext
		put("java.lang.String", "String");
		
		// int, integer, tinyint, smallint, mediumint
		put("java.lang.Integer", "Integer");
		
		// bigint
		put("java.lang.Long", "Long");
		
		// real, double
		put("java.lang.Double", "Double");
		
		// float
		put("java.lang.Float", "Float");
		
		// bit
		put("java.lang.Boolean", "Boolean");
		
		// decimal, numeric
		put("java.math.BigDecimal", "java.math.BigDecimal");
		
		// unsigned bigint
		put("java.math.BigInteger", "java.math.BigInteger");
	}};
	
	public String getType(String typeString) {
		return map.get(typeString);
	}
}
