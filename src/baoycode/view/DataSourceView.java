package baoycode.view;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import baoycode.model.DataSourceBean;
import baoycode.utils.DBUtils;
import baoycode.utils.I18n;
import baoycode.utils.PathUtils;
import baoycode.utils.SerializeUtils;
import baoycode.utils.ViewCreator;

@SuppressWarnings("unchecked")
public class DataSourceView extends TitleAreaDialog {

	public DataSourceView(Shell shell) {
		super(shell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		super.setMessage(I18n.datasouceTitle, IMessageProvider.INFORMATION);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NULL);
		GridLayout layout = new GridLayout(3, false);
		layout.verticalSpacing = 10;
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		conName = ViewCreator.createText(container, I18n.databaseConNameLabel);
		dbName = ViewCreator.createText(container, I18n.databaseNameLabel);
		driverClass = ViewCreator.createText(container, I18n.driverClassLabel);
		url = ViewCreator.createText(container, I18n.jdbcUrlLabel);
		username = ViewCreator.createText(container, I18n.usernameLabel);
		password = ViewCreator.createText(container, I18n.passwordLabel);
		loadDefaultConfig();
		return area;
	}

	private void loadDefaultConfig() {
		driverClass.setText("com.mysql.jdbc.Driver");
		username.setText("root");
		password.setText("root");
		url.setText("jdbc:mysql://xxx:3306/xxx?useUnicode=true&characterEncoding=UTF-8");
	}

	@Override
	protected int getShellStyle() {
		return super.getShellStyle() | SWT.MIN | SWT.MAX | SWT.RESIZE;
	}

	/**
	 * 初始化对话框
	 */
	@Override
	protected void configureShell(Shell newShell) {
		dataSourceBeans = (Set<DataSourceBean>) SerializeUtils.deserializeDecoder(PathUtils.getDataSourceFile());
		if (dataSourceBeans == null) {
			dataSourceBeans = new HashSet<DataSourceBean>();
		}
		super.configureShell(newShell);
	}

	/**
	 * OK 按钮单击后的回调方法
	 */
	@Override
	protected void okPressed() {
		dataSourceBeans.add(getDataSourceBean());
		try {
			SerializeUtils.serializeEncoder(dataSourceBeans, PathUtils.getDataSourceFile());
			MessageDialog.openInformation(getShell(), "SUCC", I18n.saveSucc);
		} catch (Exception e) {
			MessageDialog.openInformation(getShell(), "FAIL", I18n.saveFail + e.getMessage());
			e.printStackTrace();
		}

		// 执行保存
		super.okPressed();
	}

	/**
	 * cancel 按钮单击后的回调方法
	 */
	@Override
	protected void cancelPressed() {
		super.cancelPressed();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// super.createButtonsForButtonBar(parent);
		createButton(parent, IDialogConstants.OK_ID, I18n.save, true);
		createButton(parent, IDialogConstants.CANCEL_ID, I18n.cancel, false);
	}

	protected Control createButtonBar(Composite parent) {
		Composite container = (Composite) super.createButtonBar(parent);
		Button connect = this.createButton(container, 11, I18n.btnConnect, false);
		connect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Connection conn = DBUtils.getConntion(getDataSourceBean());
				if (conn == null) {
					MessageDialog.openInformation(getShell(), "FAIL", I18n.connectFail);
				} else {
					MessageDialog.openInformation(getShell(), "SUCC", I18n.connectSucc);
				}
				DBUtils.closeQuietly(conn);
			}
		});
		return container;
	}

	Text driverClass;
	Text conName;
	Text dbName;
	Text url;
	Text username;
	Text password;
	DataSourceBean dataSourceBean;
	Set<DataSourceBean> dataSourceBeans;

	public DataSourceBean getDataSourceBean() {
		return new DataSourceBean(conName.getText(), dbName.getText(), driverClass.getText(), url.getText(),
				username.getText(), password.getText());
	}
}
