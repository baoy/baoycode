package baoycode.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import baoycode.model.ColumnModel;
import baoycode.model.DataSourceBean;
import baoycode.model.TableModel;

public class DBUtils {

	public static Connection getConntion(DataSourceBean ds) {
		try {
			Class.forName(ds.getDriver()).newInstance();
			Properties prop = new Properties();
			prop.put("user", ds.getUser());
			prop.put("password", ds.getPassword());
			if ("ORACLE".equals(ds.getDataBaseType())) {
				prop.put("remarksReporting", "true");
			}
			Connection conn = DriverManager.getConnection(ds.getUrl(), prop);
			return conn;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<TableModel> getTableList(DataSourceBean ds) {
		List<TableModel> list = new ArrayList<TableModel>();
		String sql = "";
		if ("ORACLE".equals(ds.getDataBaseType())) {
			sql = "select table_name as name,comments as comments from user_tab_comments";
		} else if ("MYSQL".equals(ds.getDataBaseType())) {
			sql = "SHOW TABLE STATUS FROM `" + ds.getDbName() + "`";
		}

		Connection conn = null;
		try {
			conn = getConntion(ds);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				TableModel table = new TableModel();
				table.setName(rs.getString("NAME").toLowerCase());
				if ("ORACLE".equals(ds.getDataBaseType())) {
					table.setComment(rs.getString("comments"));
				} else if ("MYSQL".equals(ds.getDataBaseType())) {
					table.setComment(rs.getString("comment"));
				}
				list.add(table);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeQuietly(conn);
		}
		return list;
	}

	public static List<ColumnModel> getColumnList(DataSourceBean ds, String tableName) {
		Connection conn = null;

		List<ColumnModel> list = new ArrayList<ColumnModel>();

		String sql = "";
		if ("ORACLE".equals(ds.getDataBaseType())) {
			StringBuffer buf = new StringBuffer();
			buf.append(
					"select a.COLUMN_NAME as FIELD, c.COMMENTS as COMMENTS,A.DATA_LENGTH,A.DATA_PRECISION,A.DATA_SCALE,a.DATA_TYPE,a.NULLABLE ");
			buf.append("  from user_tab_columns a, user_col_comments c   ");
			buf.append(" where a.TABLE_NAME = c.TABLE_NAME(+)            ");
			buf.append("   and a.COLUMN_NAME = c.COLUMN_NAME(+)          ");
			buf.append("   and a.TABLE_NAME = '").append(tableName).append("' ORDER BY COLUMN_ID ASC");
			sql = buf.toString();
		} else if ("MYSQL".equals(ds.getDataBaseType())) {
			sql = "show full fields from `" + tableName + "`";
		}

		try {
			conn = getConntion(ds);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ColumnModel column = new ColumnModel();
				column.setColumnName(rs.getString("FIELD").toLowerCase());

				if ("MYSQL".equals(ds.getDataBaseType())) {
					column.setIsParamKey(rs.getString("KEY"));
					column.setComment(rs.getString("COMMENT"));
					column.setJavaType(rs.getString("TYPE"));
				} else if ("ORACLE".equals(ds.getDataBaseType())) {
					column.setDbType(rs.getString("DATA_TYPE"));

					if (rs.getInt("DATA_PRECISION") > 0) {
						// 如果是数字，此值会大于0
						column.setDatasize(rs.getInt("DATA_PRECISION"));
						column.setDigits(rs.getInt("DATA_SCALE"));
					} else {
						// 如果是字符串，需要取DATA_LENGTH值
						column.setDatasize(rs.getInt(("DATA_LENGTH")));
					}

					column.setComment(rs.getString("COMMENTS"));
					column.setIsNull(rs.getString("NULLABLE"));
					column.setIsParamKey("N");
				}

				list.add(column);
			}
			rs.close();

			if ("ORACLE".equals(ds.getDataBaseType())) {
				// 获取主键
				StringBuffer buf = new StringBuffer();
				buf.append("select cu.COLUMN_NAME                            ");
				buf.append("  from user_cons_columns cu, user_constraints au ");
				buf.append(" where cu.constraint_name = au.constraint_name   ");
				buf.append("   and au.constraint_type = 'P'                  ");
				buf.append("   and au.table_name = '").append(tableName).append("'");

				ResultSet rspk = stmt.executeQuery(buf.toString());
				while (rspk.next()) {
					String columnName = rspk.getString("COLUMN_NAME");
					for (ColumnModel column : list) {
						if (columnName.equalsIgnoreCase(column.getColumnName())) {
							column.setIsParamKey("PRI");
							break;
						}
					}
				}
				rspk.close();
			}

			for (ColumnModel column : list) {
				if ("MYSQL".equals(ds.getDataBaseType())) {
					column.init();
				} else if ("ORACLE".equals(ds.getDataBaseType())) {
					column.initOra();// 针对ORACLE进行初始化
				}
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeQuietly(conn);
		}
		return null;
	}

	public static String generateSQL(List<ColumnModel> cms, String tableName, int type) {
		StringBuffer sql = new StringBuffer();
		if (I18n.SELECT == type) {
			sql.append("select ");
			for (ColumnModel cm : cms) {
				sql.append(cm.getColumnName()).append(",");
			}
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" from ").append(tableName);
		} else if (I18n.UPDATE == type) {
			sql.append("update ").append(tableName).append(" set ");
			for (ColumnModel cm : cms) {
				sql.append(cm.getColumnName()).append(" = ? ").append(",");
			}
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" where id = ? ");
		} else if (I18n.INSERT == type) {
			StringBuffer args = new StringBuffer();
			sql.append("insert into ").append(tableName).append(" (");
			for (ColumnModel cm : cms) {
				sql.append(cm.getColumnName()).append(",");
				args.append("?,");
			}
			sql.deleteCharAt(sql.length() - 1);
			args.deleteCharAt(args.length() - 1);
			sql.append(") values (").append(args).append(")");
		} else if (I18n.INSERT_EXT == type) {
			StringBuffer args = new StringBuffer();
			sql.append("insert into ").append(tableName).append(" (");
			for (ColumnModel cm : cms) {
				sql.append(cm.getColumnName()).append(",");
				args.append(":").append(cm.getColumnName()).append(",");
			}
			sql.deleteCharAt(sql.length() - 1);
			args.deleteCharAt(args.length() - 1);
			sql.append(") values (").append(args).append(")");
		} else if (I18n.UPDATE_EXT == type) {
			sql.append("update ").append(tableName).append(" set ");
			for (ColumnModel cm : cms) {
				sql.append(cm.getColumnName()).append(" = :").append(cm.getColumnName()).append(",");
			}
			sql.deleteCharAt(sql.length() - 1);
			sql.append(" where id = :id ");
		}
		return sql.toString().toUpperCase();
	}

	public static void closeQuietly(Connection conn) {
		try {
			close(conn);
		} catch (SQLException e) { // NOPMD
			// quiet
		}
	}

	public static void close(Connection conn) throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	
	
	public static String[] getTables(DataSourceBean ds) {
		List<String> list = new ArrayList<String>();
		String sql = "";
		if ("ORACLE".equals(ds.getDataBaseType())) {
			sql = "select table_name as name,comments as comments from user_tab_comments";
		} else if ("MYSQL".equals(ds.getDataBaseType())) {
			sql = "SHOW TABLE STATUS FROM `" + ds.getDbName() + "`";
		}

		Connection conn = null;
		try {
			conn = getConntion(ds);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				list.add(rs.getString("NAME").toLowerCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeQuietly(conn);
		}
		return list.toArray(new String[list.size()]);
	}
}
