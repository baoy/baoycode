package baoycode.utils;

import java.io.File;

public class PathUtils {

	public static final String projectPath = System.getProperty("user.dir") + File.separator + "baoycode"
			+ File.separator;
	public static String dataSourceFileName = "dataSource.xml";

	public static String getDataSourceFile() {
		return projectPath + dataSourceFileName;
	}
	
}
