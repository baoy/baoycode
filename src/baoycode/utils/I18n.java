package baoycode.utils;

public interface I18n {

	public static final String databaseConNameLabel = "连接名称";
	public static final String databaseNameLabel = "数据库名称";
	public static final String usernameLabel = "用户名";
	public static final String passwordLabel = "密码";
	public static final String driverClassLabel = "数据库驱动";
	public static final String jdbcUrlLabel = "连接URL";
	
	
	public static final String packageName = "包名";
	public static final String dic = "目录";
	public static final String tpl = "模板";
	public static final String choose = "选择";

	public static final String btnConnect = "测试连接";
	public static final String btnInsert = "生成insert";
	public static final String btnInsertExt = "生成insertExt";
	public static final String btnUpdate = "生成update";
	public static final String btnUpdateExt = "生成updateExt";
	public static final String btnSelect = "生成select";
	public static final String btnFormat = "生成format";
	public static final String btnGen = "生成代码";

	public static final String saveSucc = "操作成功!";
	public static final String saveFail = "操作失败!";
	public static final String connectSucc = "连接成功！";
	public static final String connectFail = "连接失败！";
	public static final String alertCombo = "请选择模板";

	public static final String datasouceTitle = "请配置您的数据源";
	public static final String sqlTitle = "SQL生成中心";

	public static final String save = "保存";
	public static final String cancel = "取消";
	
	
	public static int SELECT = 0;
	public static int INSERT = 1;
	public static int INSERT_EXT = 3;
	public static int UPDATE = 2;
	public static int UPDATE_EXT = 4;

}
